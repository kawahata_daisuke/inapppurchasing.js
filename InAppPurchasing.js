/**
 * アプリ内課金のネイティブAPIとのJSインターフェース参考書です。
 * This reference documents the JS interface to native
 * In-App Purchasing APIs.
 *
 * @namespace
 */

var InAppPurchasing =
{
	/**
	 * @private
	 * @type {String}
	 * @constant
	 */
	CLASS_TAG : "InAppPurchasing",
	
	/**
	 * 商品情報を公開するクラス
	 * Contains information about a product
	 *
	 * @class ProductDetails
	 * @property {String} productID
	 * @property {String} price
	 * @property {String} title
	 * @property {String} description
	 * @property {String} type				Androidのみ
	 * @property {String} priceLocale		Appleのみ
	 * @property {Boolean} downloadable		Appleのみ
	 */
	ProductDetails : function()
	{
		this.productID = null;
		this.price = null;
		this.title = null;
		this.description = null;
		this.type = null;
		this.priceLocale = null;
		this.downloadable = null;
	},

	/**
	 * 商品情報返事データを公開するクラス
	 * Contains response data for a product details requset
	 *
	 * @class ProductDetailsResponse
	 * @property {Array} productsDetails 取得成功の諸商品データ
	 * @property {Array} failedProductIDs 取得失敗の諸商品識別子
	 */
	ProductsDetailsResponse : function()
	{
		this.productsDetails = null;
		this.failedProductIDs = null;
	},
	
	/**
	 * 完了した購入に当たる情報
	 * Contains information about a completed purchase
	 *
	 * @class PurchaseDetails
	 * @property {Boolean} succeeded
	 * @property {String} productID
	 * @property {String} transactionID
	 * @property {String} transactionReceipt
	 * @property {String} transactionSignature
	 * @property {Number} transactionDate UNIX時間 (例：1361031927)
	 * @property {String} payload
	 */
	PurchaseDetails : function()
	{
		this.succeeded = null;
		this.productID = null;
		this.transactionID = null;
		this.transactionReceipt = null;
		this.transactionSignature = null;
		this.transactionDate = null;
		this.payload = null;
	},

	/**
	 * 消費要求の返事を公開するクラス
	 * Response for a consumption request
	 *
	 * @class ProductDetailsResponse
	 * @property {Array} productsDetails 消費成功の諸商品データ
	 * @property {Array} failedProductIDs 消費失敗の諸商品識別子
	 */
	ProductsConsumeResponse : function()
	{
		this.productsDetails = null;
		this.failedProductIDs = null;
	},

	/**
	 * InAppPurchasingの初期化を行う
	 * Initialize InAppPurchasing
	 *
	 * @param {Function} successCallback 正常終了時に命令するコールバック
	 * @param {Function} errorCallback 異常終了時に命令するコールバック
	 */
	initialize : function(successCallback, errorCallback)
	{
		cordova.exec(successCallback, errorCallback,
				this.CLASS_TAG, "initialize", []);
		return;
	},

	/**
	 * initializeの正常完了コールバック
	 * Callback on successful initialization
	 *
	 * @event
	 */
	handleInitializationCompletion : function(){},

	/**
	 * initializeの異常終了コールバック
	 * Callback on initialization error
	 *
	 * @event
	 * @param {String} error エラーの詳細
	 */
	handleInitializationError : function(error){},

	/**
	 * 未完了購入を公開する要求
	 * Request pending purchases
	 *
	 * @param {Function} successCallback 正常終了時に命令するカールバック
	 * @param {Function} errorCallback 以上終了時に命令するカールバック
	 */
	requestPendingPurchases : function(successCallback, errorCallback)
	{
		cordova.exec(successCallback, errorCallback,
				this.CLASS_TAG, "requestPendingPurchases", []);
		return;
	},

	/**
	 * requestPendingPurchaseの正常完了カールバック
	 * Callback on successful completion of requestPendingPurchases
	 *
	 * @event
	 * @param {Array} pendingPurchases 未完了購入の商品識別子配列
	 */
	handlePendingPurchases : function(pendingPurchases){},

	/**
	 * requestPendingPurchaseの異常完了カールバック
	 * Callback on requestPendingPurchases error
	 *
	 * @event
	 */
	handlePendingPurchasesError: function(error){},
	
	/**
	 * 商品詳細の要求―価格など
	 * Request details about a product--price, etc.
	 * 
	 * @param {Array} productIDs 商品識別子を含める列―Google PlayのSKUなど
	 * @param {Function} successCallback 正常終了時に命令するコールバック
	 * @param {Function} errorCallback 異常終了時に命令するコールバック
	 */
	requestDetailsForProducts : function(productIDs, successCallback,
										errorCallback)
	{
		cordova.exec(successCallback, errorCallback,
				this.CLASS_TAG, "requestDetailsForProducts",
				[productIDs]);
		return;
	},
	
	/**
	 * requestDetailsForProductsの正常終了コールバック
	 * Callback on successful completion of requestDetailsForProducts
	 *
	 * @event
	 * @param {ProductsDetailsResponse} response
	 */
	handleProductsDetailsResponse : function(response){},
	
	/**
	 * requestDetailsForProductsの異常終了コールバック
	 * Callback on error in requestDetailsForProducts
	 *
	 * @event
	 * @param {String} error エラーの詳細
	 */
	handleProductsDetailsError : function(error){},
	
	/**
	 * 商品購入の要求
	 * Request payment for a product
	 *
	 * @param {String} productID 商品識別子
	 * @param {Boolean} consumable 多重購入の有無効。trueの場合、有効
	 * @param {String} payload 文字列の型にした任意ﾃﾞｰﾀ。そのままPurchaseDetails#payloadに渡す
	 * @param {Function} successCallback 正常終了時に命令するコールバック
	 * @param {Function} errorCallback 異常終了時に命令するコールバック
	 */
	requestPaymentForProduct : function(productID,
										consumable,
										payload,
										successCallback,
										errorCallback)
	{
		cordova.exec(successCallback, errorCallback,
				this.CLASS_TAG, "requestPaymentForProduct",
				[productID, consumable, payload]);
	},
	
	/**
	 * requestPaymentForProductの正常終了コールバック
	 * Callback on successful completion of requestPaymentForProduct
	 *
	 * @event
	 * @param {PaymentDetails} paymentDetails
	 */
	handleProductsPaymentResponse : function(paymentDetails){},
	
	/**
	 * prequestPaymentForProducの異常終了コールバック
	 * Callback on error in requestPaymentForProduct
	 *
	 * @event
	 * @param {String} error エラーの詳細
	 */
	handleProductsPaymentError : function(error){},

	/**
	 * 商品消費の要求
	 * Request product consumption
	 *
	 * @param {Array} productID 商品識別子の配列
	 * @param {Function} successCallback 正常終了時に命令するコールバック
	 * @param {Function} errorCallback 異常終了時に命令するコールバック
	 */
	requestConsumeProducts : function(productIDs, successCallback,
									errorCallback)
	{
		cordova.exec(successCallback, errorCallback,
				this.CLASS_TAG, "requestConsumeProducts",
				[productIDs]);
	},
	
	/**
	 * requestConsumeProducts の正常終了コールバック
	 * Callback on successful completion of requestConsumeProduct
	 *
	 * @event
	 * @param {ProductsConsumeResponse} response
	 */
	handleConsumeProductsResponse : function(response){},
	
	/**
	 * requestConsumeProduct の異常終了コールバック
	 * Callback on error in requestConsumeProduct
	 *
	 * @event
	 * @param {String} error エラーの詳細
	 */
	handleConsumeProductsError : function(error){}
};

